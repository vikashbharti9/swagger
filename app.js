const express = require('express');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const app = express();

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Customer API',
            version: '1.0.0',
            description: "Customer API Information",
            contact: {
                name: "Vikash Bharti"
            },
        }
    },
    apis: ['app.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
console.log(swaggerDocs);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs));

/**
 * @swagger
 * /customers:
 *   get:
 *     description: Get all customers
 *     responses:
 *       '200':
 *         description: Successful Response
 */

app.get('/customers', (req, res) => {
    res.send([
        {
            id: 1,
            title: 'Harry'
        }
    ])
});

/**
 * @swagger
 * /customers/{id}:
 *   get:
 *     description: Get customers by id
 *     parameters:
 *     - name: id
 *       description: id to get by
 *       in: path
 *       type: integer
 *       required: true
 *     responses:
 *       '200':
 *         description: Successful Response
 */


app.get('/customers/:id', (req, res) => {
    res.status(200).json({
        getID: req.params.id
    })
});

/**
 * @swagger
 * /customers/{id}:
 *   delete:
 *     description: Delete customers by id
 *     parameters:
 *     - name: id
 *       description: id to Delete by
 *       in: path
 *       type: integer
 *       required: true
 *     responses:
 *       '200':
 *         description: Successful Response
 */

app.delete('/customers/:id', (req, res) => {
    res.status(200).json({
        deleteID: req.params.id
    })
});

/**
 * @swagger
 * /customers/{id}:
 *   patch:
 *     description: Patch by id with req body
 *     parameters:
 *     - name: id
 *       description: id to update
 *       in: path
 *       type: integer
 *       required: true
 *     - name: reqBody
 *       description: request body
 *       in: body
 *       schema:
 *         type: object
 *         properties:
 *           itemName:
 *             type: string
 *           itemDescription:
 *             type: string
 *         required:
 *           - itemName
 *           - itemDescription
 *     responses:
 *       '200':
 *         description: Successful Response
 */

app.patch('/customers/:id', express.json(), (req, res) => {
    res.status(200).json({
        patchID: req.params.id,
        newItemName: req.body.itemName,
        newItemDescription: req.body.newItemDescription
    })
});

/**
 * @swagger
 * /customer:
 *   post:
 *     description: Create a new customer
 *     parameters:
 *     - name: title
 *       description: title of the customer
 *       in: formData
 *       required: true
 *       type: string
 *     responses:
 *       201:
 *         description: Created
 * 
 */

app.post('/customer', (req, res) => {
    res.status(201).send();
});


app.listen(5000, () => console.log("listening on 5000"));
